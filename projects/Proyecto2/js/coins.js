/** @type { import "../modules/apitypes.js"; } */

const searchParams = {
  get byName() {
    return jQuery('input#cripto-name').text()
  },
  get filterBy() {
    return jQuery('.dropdown-menu#dd-filter-menu a.active').attr('value')
  },
  get orderBy() {
    return jQuery('.dropdown-menu#dd-order-menu a.active').attr('value')
  },
};

const progressBar = jQuery('#progress-bar');

const emptyMessage = jQuery('#empty-text');

const pageNumber =  jQuery('#page-number');

const coinHolder = jQuery('div#coin-box')

let currentPage = 1;

const baseAPIUrl = 'https://api.coingecko.com/api/v3';

(async function ($) {
  $('#search-form').submit((e) => { e.preventDefault() })
  $('#dd-filter-menu a').click(onDropdownMenuSelected)
  $('#dd-order-menu a').click(onDropdownMenuSelected)
  $('#btnNext').click(onNextClicked)
  $('#btnPrev').click(onPrevClicked)
  console.info('Intializing coin webpage', searchParams)
  onSearch()
})(jQuery);


function onSearch(avoidByName=false) {
  const $ = jQuery
  emptyMessage.addClass('d-none')
  progressBar.removeClass('d-none')
  coinHolder.empty()
  let requestUrl = `${baseAPIUrl}/coins/markets?vs_currency=usd&order=${searchParams.filterBy}_${searchParams.orderBy}&page=${currentPage}&per_page=20`
  if (!avoidByName && searchParams.byName !== ''){
    requestUrl += encodeURI(`&ids=${searchParams.byName.replaceAll(/\s+/g, ',')}`)
  }
  fetch(requestUrl)
  .then(it => it.json())
  .then( /** @param {CoinMarketData[]} response */response => {
    console.info('received: ', response)
    if (!response) {
      progressBar.addClass('d-none')
      emptyMessage.removeClass('d-none')
      return
    }
    if (response.length < 20) {
      $('.btn#btnNext').addClass('disabled')
    } else if (currentPage > 1){
      $('.btn#btnPrev').removeClass('disabled')
    }
    response.forEach(coin => {
      const template = $(`
      <div class="col-12 col-md-4 col-xl-3 p-2">
        <div class="card bg-dark p-2 w-100" id="${coin.id}">
          <img src="${coin.image}" alt="bitcoin-logo" class="mh-50 mw-100card-img-top d-none d-sm-grid mx-4">
          <div class="card-body">
            <h5 class="card-title">${coin.name}</h5>
            <span>Símbolo: ${coin.symbol}</span><br>
            <span>Precio actual: $${coin.current_price || '0.000'}</span><br>
            <span>Variación:</span><br>
            <span>
              <small class="text-success">
              <i class="bi bi-arrow-up"></i>$${coin.high_24h || '0.000'}</small>
              <small class="text-danger">
              <i class="bi bi-arrow-down"></i>$${coin.low_24h || '0.000'}</small>
            </span>
          </div>
        </div>
      </div>`)
      coinHolder.append(template)
    })
    progressBar.addClass('d-none')
    emptyMessage.addClass('d-none')
    pageNumber.html(currentPage)
  })
  .catch(e => {
    console.error(e)
    emptyMessage.removeClass('d-none')
    progressBar.addClass('d-none')
  })
  console.log('onSearch clicked', searchParams)
}

function onNextClicked() {
  currentPage++
  onSearch()
}

function onPrevClicked() {
  if (currentPage === 1) return
  currentPage--
  onSearch()
  console.log('Stub!')
}
function onDropdownMenuSelected() {
  const $ = jQuery
  $(this).parents('.dropdown').find('.btn').html($(this).text())
  $(this).parents('.dropdown-menu').find('.dropdown-item').removeClass('active')
  $(this).addClass('active')
  onSearch(true)
}


