/** @type { import "../modules/apitypes.js"; } */
/** @type { import Chart from "../lib/chart/chart.min.js" } */
(async function ($) {
    "use strict";
    // Chart Global Color
    Chart.defaults.color = "#6C7293";
    Chart.defaults.borderColor = "#000000";
    // if (Math.min(2, 3)) return
    const baseAPIUrl = 'https://api.coingecko.com/api/v3'
    /** @type {CoinMarketData[]} */
    // const topCoinList = await fetch(`./db/coinMarketData.json`).then(it => it.json())
    const topCoinList = await fetch(`${baseAPIUrl}/coins/markets?vs_currency=usd&per_page=7&sparkline=true`).then(it => it.json())
    /** @type {ExchangeData[]} */
    // const topExchangeList = await fetch('./db/exchangeData.json').then(it => it.json())
    const topExchangeList = await fetch(`${baseAPIUrl}/exchanges?per_page=7`).then(it => it.json())
    /** @type {HTMLDivElement} */
    const topCoinContainer = $('#top-cripto')
    const lineListOptions = {
      plugins: {
        legend: {
          display: false
        }
      },
      elements: {
        point: {
          radius: 0,
          pointStyle: 'line'
        },
        line: {
          tension: 1,
          fill: true,
          borderJoinStyle: 'bevel',
          borderWidth: 0.5
        }
      },
      responsive: true,
      indexAxis: 'x',
      scales: {
        yAxes: [{ gridLines: { display: false }, ticks: { callback: (val) => val.toExponential()} }]
      }
    }

    console.info('Starting rendering the top coin list')
    topCoinList.forEach(coin => {
      const coinCard = $(
        `<div class="container hover-overlay hover-zoom hover-shadow ripple m-1 p-1">
          <div class="row justify-content align-items-center p-1 m-1">
            <div class="col-2 p-0">
              <img class="w-100" src="${coin.image}">
            </div>
            <!-- The Coin info section -->
            <div class="col-4 col-lg-5">
                <h6><small class="text-info">#${coin.market_cap_rank} </small>${coin.name} (<span>${coin.symbol}</span>)</h6>
              <span class="text-warning"><i class="bi bi-currency-dollar"></i>${coin.current_price}</span> <br>
              <small class="text-success"><i class="bi bi-arrow-up"></i>$${Math.round(coin.high_24h * 1e3) / 1e3}</small>
              <small class="text-danger"><i class="bi bi-arrow-down"></i>$${Math.round(coin.low_24h * 1e3) / 1e3}</small>
            </div>
            <div class="col-5 p-0">
              <canvas id="${coin.id}-canvas">
            </div>
          </div>
        </div>`
      )
      topCoinContainer.append(coinCard)
      const graphCtx = $(`#${coin.id}-canvas`)
      const chart = new Chart(graphCtx, {
        type: 'line',
        data: {
          labels: coin.sparkline_in_7d.price.map(_ => ''),
          datasets: [{
            label: 'Precio los últimos 7 días',
            data: coin.sparkline_in_7d.price,
            fill: true,
            borderColor: 'rgb(75, 192, 192)',
          }],
        },
        options: lineListOptions,
      })
    })
    const topExchangeContainer = $('#top-exchanges')
    console.info('Starting rendering the top exchange list')
    topExchangeList.forEach( async exchange => {
      const exchangeCard = $(
        `<div class="container m1 p-1 justify-content-center">
          <div class="row mx-3">
            <div class="col-2 p-0">
              <img class="w-100 p-2" src="${exchange.image}">
            </div>
            <!-- Information of the exchange -->
            <div class="col-5 align-content-left p-0">
              <div class="w-100">
                <div class="w-100">
                  <h7 class="text-left"><a href="${exchange.url}">${exchange.name}</a></h7>
                  <span>${exchange.id}</span>
                </div>
                <div>
                  <small>Trust Score: ${exchange.trust_score}/10</small>
                  <small>Rank: ${exchange.trust_score_rank}</small>
                </div>
                <div>
                  <small>${exchange.country} (${exchange.year_established})</small>
                </div>
              </div>
            </div>
            <!-- Chart-->
            <div class="col-5 p-0">
              <canvas id="${exchange.id}-volume-chart">
            </div>
          </div>
        </div>`
      )
      topExchangeContainer.append(exchangeCard)
      const volumeCtx = $(`canvas#${exchange.id}-volume-chart`)
      const volumeData = await fetch(`${baseAPIUrl}/exchanges/binance/volume_chart?days=7`).then(it => it.json())
      new Chart(volumeCtx, {
        type: 'line',
        data: {
          labels: volumeData.map(it => ''),
          datasets: [{
            data: volumeData.map(it => it[0]),
            fill: true,
            borderColor: 'rgb(75, 192, 192)',
          }],
        },
        options: {
          ...lineListOptions
          // ...(_.set(lineListOptions, 'scales.yAxes[0].ticks.callback', (val)=> val.toExponential())),

        },
      })
    })

    console.info('End of rendering fetched content')

    $('#spinner').removeClass('show')

    // Spinner
    // var spinner = function () {
    //     setTimeout(function () {
    //         if ($('#spinner').length > 0) {
    //             $('#spinner').removeClass('show');
    //         }
    //     }, 1);
    // };
    // spinner();
    console.log('[Start] fetching data...')
    // Promise.all([
    //   fetch(`${baseAPIUrl}/coins/list`),
    //   fetch(`${baseAPIUrl}/exchanges?per_page=3`)
    // ])
    // .then(it => it.map(x => x.json()))
    // // .then( it => it.json())
    // .then(([coins, exchanges]) => {
    //     // coinList = jsonData
    //     console.debug('[Start] Loaded coin list: ', coins)
    //     console.debug('[Start] Loaded exchange list: '. exchanges)
    //     render($, coins.value, exchanges.value)
    // })
    // .finally(() => { $('#spinner').removeClass('show') })


    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });
    $('.back-to-top').click(function () {
        $('html, body').animate({scrollTop: 0}, 1500, 'easeInOutExpo');
        return false;
    });


    // Sidebar Toggler
    $('.sidebar-toggler').click(function () {
        $('.sidebar, .content').toggleClass("open");
        return false;
    });


    // Progress Bar
    $('.pg-bar').waypoint(function () {
        $('.progress .progress-bar').each(function () {
            $(this).css("width", $(this).attr("aria-valuenow") + '%');
        });
    }, {offset: '80%'});


    // Calender
    $('#calender').datetimepicker({
        inline: true,
        format: 'L'
    });


    // Testimonials carousel
    $(".testimonial-carousel").owlCarousel({
        autoplay: true,
        smartSpeed: 1000,
        items: 1,
        dots: true,
        loop: true,
        nav : false
    });





    // Worldwide Sales Chart
    // var ctx1 = $("#worldwide-sales").get(0).getContext("2d");
    // var myChart1 = new Chart(ctx1, {
    //     type: "bar",
    //     data: {
    //         labels: ["2016", "2017", "2018", "2019", "2020", "2021", "2022"],
    //         datasets: [{
    //                 label: "USA",
    //                 data: [15, 30, 55, 65, 60, 80, 95],
    //                 backgroundColor: "rgba(235, 22, 22, .7)"
    //             },
    //             {
    //                 label: "UK",
    //                 data: [8, 35, 40, 60, 70, 55, 75],
    //                 backgroundColor: "rgba(235, 22, 22, .5)"
    //             },
    //             {
    //                 label: "AU",
    //                 data: [12, 25, 45, 55, 65, 70, 60],
    //                 backgroundColor: "rgba(235, 22, 22, .3)"
    //             }
    //         ]
    //         },
    //     options: {
    //         responsive: true
    //     }
    // });


    // Salse & Revenue Chart
    // var ctx2 = $("#salse-revenue").get(0).getContext("2d");
    // var myChart2 = new Chart(ctx2, {
    //     type: "line",
    //     data: {
    //         labels: ["2016", "2017", "2018", "2019", "2020", "2021", "2022"],
    //         datasets: [{
    //                 label: "Salse",
    //                 data: [15, 30, 55, 45, 70, 65, 85],
    //                 backgroundColor: "rgba(235, 22, 22, .7)",
    //                 fill: true
    //             },
    //             {
    //                 label: "Revenue",
    //                 data: [99, 135, 170, 130, 190, 180, 270],
    //                 backgroundColor: "rgba(235, 22, 22, .5)",
    //                 fill: true
    //             }
    //         ]
    //         },
    //     options: {
    //         responsive: true
    //     }
    // });



    // Single Line Chart
    // var ctx3 = $("#line-chart").get(0).getContext("2d");
    // var myChart3 = new Chart(ctx3, {
    //     type: "line",
    //     data: {
    //         labels: [50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150],
    //         datasets: [{
    //             label: "Salse",
    //             fill: false,
    //             backgroundColor: "rgba(235, 22, 22, .7)",
    //             data: [7, 8, 8, 9, 9, 9, 10, 11, 14, 14, 15]
    //         }]
    //     },
    //     options: {
    //         responsive: true
    //     }
    // });


    // Single Bar Chart
    // var ctx4 = $("#bar-chart").get(0).getContext("2d");
    // var myChart4 = new Chart(ctx4, {
    //     type: "bar",
    //     data: {
    //         labels: ["Italy", "France", "Spain", "USA", "Argentina"],
    //         datasets: [{
    //             backgroundColor: [
    //                 "rgba(235, 22, 22, .7)",
    //                 "rgba(235, 22, 22, .6)",
    //                 "rgba(235, 22, 22, .5)",
    //                 "rgba(235, 22, 22, .4)",
    //                 "rgba(235, 22, 22, .3)"
    //             ],
    //             data: [55, 49, 44, 24, 15]
    //         }]
    //     },
    //     options: {
    //         responsive: true
    //     }
    // });


    // Pie Chart
    // var ctx5 = $("#pie-chart").get(0).getContext("2d");
    // var myChart5 = new Chart(ctx5, {
    //     type: "pie",
    //     data: {
    //         labels: ["Italy", "France", "Spain", "USA", "Argentina"],
    //         datasets: [{
    //             backgroundColor: [
    //                 "rgba(235, 22, 22, .7)",
    //                 "rgba(235, 22, 22, .6)",
    //                 "rgba(235, 22, 22, .5)",
    //                 "rgba(235, 22, 22, .4)",
    //                 "rgba(235, 22, 22, .3)"
    //             ],
    //             data: [55, 49, 44, 24, 15]
    //         }]
    //     },
    //     options: {
    //         responsive: true
    //     }
    // });


    // Doughnut Chart
    // var ctx6 = $("#doughnut-chart").get(0).getContext("2d");
    // var myChart6 = new Chart(ctx6, {
    //     type: "doughnut",
    //     data: {
    //         labels: ["Italy", "France", "Spain", "USA", "Argentina"],
    //         datasets: [{
    //             backgroundColor: [
    //                 "rgba(235, 22, 22, .7)",
    //                 "rgba(235, 22, 22, .6)",
    //                 "rgba(235, 22, 22, .5)",
    //                 "rgba(235, 22, 22, .4)",
    //                 "rgba(235, 22, 22, .3)"
    //             ],
    //             data: [55, 49, 44, 24, 15]
    //         }]
    //     },
    //     options: {
    //         responsive: true
    //     }
    // });


})(jQuery);


/**
 *
 * @param {ExchangeData} data
 */
function exchangeItem(data){
  return `
    <div>
      <img src="${data.image}">
      <div>
        <h3>${data.name}</h3>
        <h6>score: ${data.trust_score}/10</h6>
        <small>Trust Rank:${data.trust_score_rank}</small>
      </div>
    </div>
  `
}

/**
 * @class Component
 * @property {Object} bindings The dictinary of the bindings of the current component
 * @constructor
*/
function Component(init = { parentSelector: null, id: '' }) {
  const uid = Math.exp(Math.PI, Math.random() * Math.E)
  Component.prototype.bindings = {}
}

/**
 *
 * @param {jQuery} $
 * @param {Array<CoinData>} topCoinList
 * @param {Array<ExchangeData>} topExchangeList
 */
function render($, topCoinList, topExchangeList) {
  /** @type {HTMLDivElement?} */
  const topCripto = $('#top-cripto')
  topCripto.innerHTML = ''
  /** @type {HTMLDivElement?} */
  const topExchange = $('#top-exchanges')
  topCoinList.forEach(coin => {
    console.info('[topCoinList] drawing and item: (', coin, ')')
    topCripto.innerHTML += exchangeItem(coin)
  });
}



