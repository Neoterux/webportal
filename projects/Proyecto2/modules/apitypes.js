/**
 * @typedef {Object} CoinData
 * @property {string} id the Id of the coin
 * @property {string} symbol The symbol of the coin
 * @property {string} name The full name of the Coin
 *
 * @typedef {Object} ITrendingItem
 * @property {number} coin_id The id of the coin in number
 *
 * @typedef {CoinData & ITrendingItem} TrendingItem
 *
 * @typedef {Object} TrendingItemHolder
 * @property {TrendingItem} item
 *
 * @typedef {Object} ExchangeData
 * @property {string} id the id of the given exchagne
 * @property {string} name The name of the given exchange
 * @property {number} year_established The year when
 * @property {string} country The name of the country
 * @property {string} url The url of the exchange
 * @property {string} image The url of the logo
 * @property {number} trust_score The trust score of the exchange
 * @property {number} trust_score_rank The
 *
 * @typedef {Object} PriceSparkLine
 * @property {number[]} price
 *
 * @typedef {Object} CoinMarketData
 * @property {string} id
 * @property {string} symbol
 * @property {string} name
 * @property {string} image
 * @property {number} current_price
 * @property {number} market_cap
 * @property {number} high_24h
 * @property {number} low_24h
 * @property {number} market_cap_rank
 * @property {PriceSparkLine?} sparkline_in_7d
 */

