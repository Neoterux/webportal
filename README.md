# WebPortal

Portal de proyectos para la materia Desarrollo de aplicaciones web y móviles

### [Link to Web Portal](https://neoterux.gitlab.io/webportal/)

### Author:

- [Luis Andrés Bajaña Fernandez](https://gitlab.com/Neoterux)

## Project status

- [1st Project](https://neoterux.gitlab.io/webportal/projects/Proyecto1)
- [2nd Project](https://neoterux.gitlab.io/webportal/projects/Proyecto2)
